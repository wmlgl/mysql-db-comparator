# mysql-db-comparator

#### 项目介绍
mysql数据库对比工具，用来对比更新开发与生产数据库

#### 使用说明

Config:   

	SqlDBStructCompareTool.properties   
	dev是开发环境数据库   
	pro是正式环境数据库   
	
Command:  

	java -jar ./SqlDBStructCompareTool.jar ./SqlDBStructCompareTool.properties  
	
说明:

	1.本工具会自动列出数据库表、字段、索引的新增、修改和删除  
	2.本工具会自动生成数据库表、字段、索引的新增、修改和删除的sql语句，删除语句都会加上注释， 索引的修改是通过先删除在新增来实现。 

#### 参与贡献

1. Fork 本项目
2. 新建 mysql-db-comparator_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)