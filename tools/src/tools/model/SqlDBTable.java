package tools.model;

import java.util.List;

public class SqlDBTable {
	private String tableName;
	private String tableCreate;
	private List<SqlDBColumn> columns;
	private List<SqlDBIndex> indexes;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<SqlDBColumn> getColumns() {
		return columns;
	}

	public void setColumns(List<SqlDBColumn> columns) {
		this.columns = columns;
	}

	public String getTableCreate() {
		return tableCreate;
	}

	public void setTableCreate(String tableCreate) {
		this.tableCreate = tableCreate;
	}

	public List<SqlDBIndex> getIndexes() {
		return indexes;
	}

	public void setIndexes(List<SqlDBIndex> indexes) {
		this.indexes = indexes;
	}
}
