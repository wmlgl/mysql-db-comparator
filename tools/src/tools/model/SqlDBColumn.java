package tools.model;

public class SqlDBColumn {
	private String tableName;
	private String field;
	private String type;
	private boolean isNull;
	private String key;
	private String defaultValue;
	private String extra;
	private String comment;
	
	private boolean isDelete;
	private boolean isModify;
	private boolean isCreate;

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean getNull() {
		return isNull;
	}

	public void setNull(boolean isNull) {
		this.isNull = isNull;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public boolean isModify() {
		return isModify;
	}

	public void setModify(boolean isModify) {
		this.isModify = isModify;
	}

	public boolean isCreate() {
		return isCreate;
	}

	public void setCreate(boolean isCreate) {
		this.isCreate = isCreate;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String toString() {
		if(isDelete) {
			return String.format(
					"alter table `%1$s` drop column `%2$s`;" ,
					getTableName(),
					field
				);
		}
		if(isModify) {
			return String.format(
					"alter table `%1$s` change column `%2$s` `%2$s` %3$s %4$s %5$s %6$s %7$s;" ,
					getTableName(),
					field,
					type,
					isNull ? "null" : "not null",
					defaultValue == null ? "" : "default '"+defaultValue+"'",
					comment == null ? "" : "comment '"+comment+"'",
					extra == null ? "" : extra
				);
		}
		return String.format(
				"alter table `%1$s` add column `%2$s` %3$s %4$s %5$s %6$s %7$s;" ,
				getTableName(),
				field,
				type,
				isNull ? "null" : "not null",
				defaultValue == null ? "" : "default '"+defaultValue+"'",
				comment == null ? "" : "comment '"+comment+"'",
				extra == null ? "" : extra
			);
	}
}
